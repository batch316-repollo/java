import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class activity2 {

	public static void main(String[] args) {
		String host = "https://batch288repollo.onrender.com";
		
		HttpRequest request = (HttpRequest) HttpRequest.newBuilder()
				.uri(URI.create(host + "/products/getActiveProducts"))
				.method("GET", HttpRequest.BodyPublishers.noBody())
				.build();
		
		HttpResponse<String> response = null;
		try {
			response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (InterruptedException  e) {
			e.printStackTrace();
		}
		
		System.out.println(response.body());
	}

}
