package com.zuitt.activty1;

import java.text.DecimalFormat;
import java.util.Scanner;


public class Activity1 {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		String firstName = null;
		String lastName = null;
		Double firstSubject = null;
		Double secondSubject = null;
		Double thirdSubject = null;
		Double avg = 0.0;
		
		System.out.println("Enter First Name: ");
		firstName = sc.nextLine();
		System.out.println("Enter Last Name: ");
		lastName = sc.nextLine();
		
		System.out.println("Enter First Subject Grade: ");
		firstSubject = Double.parseDouble(sc.nextLine());
		avg += firstSubject;
		System.out.println("Enter Second Subject Grade: ");
		secondSubject = Double.parseDouble(sc.nextLine());
		avg += secondSubject;
		System.out.println("Enter Third Subject Grade: ");
		thirdSubject = Double.parseDouble(sc.nextLine());
		avg += thirdSubject;
		
		sc.close();
		avg = avg / 3;
		
		DecimalFormat df = new DecimalFormat("#.##");
		System.out.println("Hi " + firstName + " " + lastName + "!");
		System.out.println("Your average grade is " + df.format(avg) + ".");
	}

}
