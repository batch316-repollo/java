import java.util.*;

public class activity1 {

	public static void main(String[] args) {
		System.out.println("Hello World!");
		
		//Problem 1:
		System.out.println(numTimesPi(4.5));
		System.out.println(numTimesPi(1));
		
		//Problem 2:		
		int[] arr = {2, 3, 4, 5, 6};
		System.out.println("[evens, odds]: " + evensOdds(arr));
		int[] arr1 = {22, 32, 42, 52, 62};
		System.out.println("[evens, odds]: " + evensOdds(arr1));
		
		//Problem 3:
		System.out.println(sumofConcatNumbers(8));
		
		//Problem 4:
		System.out.println(addPrefixAndSuffix("appleinc"));
		System.out.println(addPrefixAndSuffix("app"));
	}
	
	private static double numTimesPi(double num) {
		return Math.PI * num;
	}
	
	private static List<Integer> evensOdds(int[] numsArray) {
		List<Integer> evensOdds = new ArrayList<>();
		int evens = 0, odds = 0;
		
		for (int i : numsArray) {
			evens = i % 2 == 0 ? evens + 1 : evens;
			odds = i % 2 == 0 ? odds : odds + 1;
		}
		
		evensOdds.add(evens);
		evensOdds.add(odds);
		
		return evensOdds;
	}
	
	private static int sumofConcatNumbers(int num) {
		return num + (num * 11) + (num * 111);
	}
	
	private static String addPrefixAndSuffix(String s) {
		if (s.length() <= 3) {
			return s;
		}
		
		return s.substring(0, 3) + s + s.substring(0, 3); 
	}
}
