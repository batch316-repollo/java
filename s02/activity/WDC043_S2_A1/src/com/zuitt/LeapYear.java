package com.zuitt;

import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Integer year = 0;

        System.out.println("Input year to be checked if a leap year.");
        year = Integer.parseInt(sc.nextLine());

        if (year % 4 == 0 && year % 100 != 0) {
            System.out.println(year + " is a leap year.");
        }
        else if (year % 400 == 0 && year % 100 == 0) {
            System.out.println(year + " is a leap year.");
        }
        else {
            System.out.println(year + " is NOT a leap year.");
        }
    }
}
