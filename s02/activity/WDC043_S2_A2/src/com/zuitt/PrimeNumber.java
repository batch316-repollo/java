package com.zuitt;

public class PrimeNumber {

    public static void main (String[] args) {
        Integer[] primeNumbers = new Integer[5];
        int index = 0;

        // Get first 5 prime numbers from 0 to 100.
        for (int i = 2; i < 100; i++) {
            if (index == 5) {
                break;
            }
            else {
                for (int j = 2; j <= i; j++) {
                    if (i == j) {
                        primeNumbers[index] = i;
                        index++;
                    }
                    else if (i % j == 0) {
                        break;
                    }
                }
            }
        }

        // Print first 5 prime numbers.
        index = 0;
        String[] primeNumberIndex = {"first", "second", "third", "fourth", "fifth"};
        for (Integer primeNumber : primeNumbers) {
            System.out.println("The " + primeNumberIndex[index] + " prime number is: " + primeNumber);
            index++;
        }
    }
}
