package com.zuitt.example;

public class Dog extends Animal {
    // extends keyword allows us to have this class inherit the attributes and methods of the Animal class.
    // Parent class is the class where we inherit from.
    // Child class / subclass is a class that inherits from a parent.
    // No sub-classes can inherit from multiple parents, but can inherit from it's ancestors.
    // Instead a sub-class can "multiple inherit" from what we call interfaces.

    private String dogBreed;

    public Dog() {
        super(); // super is a reference to the parent class. super() is accessing the constructor method of the parent class.
        this.dogBreed = "Chihuahua";
    }

    public Dog(String name, String color, String dogBreed) {
        super(name, color);
        this.dogBreed = dogBreed;
    }

    public String getDogBreed() {
        return dogBreed;
    }

    public void setDogBreed(String dogBreed) {
        this.dogBreed = dogBreed;
    }

    public void greet() {
        super.call(); // super os a reference to the parent. It calls the "call()" method inherited from the Animal class.
        System.out.println("Bark!");
    }
}
