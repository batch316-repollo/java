package com.zuitt.example;

public class Car {
    // Properties / Attributes - the characteristics of the object the class will create.
    // Constructor - method to create the object and instantiate with its initialized value.
    // Getters and Setters - are methods to get values of an objects properties or set them.
    // Method - actions that an object can perform or do.

    // public access - the variable / property in the class is accessible anywhere in the application.
    // private - limits the access and ability to get or set a variable / method to only w/in its own class.
    // getters - methods that return the value of the property.
    // setters - methods that allow us to set the value of a property.

    private String make;
    private String brand;
    private int price;
    private Driver carDriver;

    // Constructor is a method w/c allows us to set the initial values of an instance.
    public Car() {
        this.carDriver = new Driver("",0);
    }

    public Car(String make, String brand, int price, Driver carDriver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = carDriver;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    // Methods are functions that allows us to execute our desired logic or code and also to return something when desired.
    public void start() {
        System.out.println(brand + " " + make + ": Vroom Vroom!");
    }

    public String getCarDriverName() {
        return carDriver.getName();
    }

    // Classes have relationships:
    /*
        Composition allows modelling objects to be made up of other objects.
        Classes can have instances of other classes.
    * */
}
