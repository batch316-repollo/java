package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Car car1 = new Car();
        System.out.println(car1.getBrand());
        System.out.println(car1.getMake());
        System.out.println(car1.getPrice());
        System.out.println();

        car1.setMake("Veyron");
        car1.setBrand("Bugatti");
        car1.setPrice(200000);
        System.out.println(car1.getBrand());
        System.out.println(car1.getMake());
        System.out.println(car1.getPrice());
        System.out.println();

        // Mini-Activity.
        Driver takumiFujiwara = new Driver("Takumi Fujiwara",21);
        Car car2 = new Car("86", "Toyota", 40000, takumiFujiwara);
        Car car3 = new Car("Impreza", "Subaru", 40000, takumiFujiwara);
        // car2
        System.out.println(car2.getBrand());
        System.out.println(car2.getMake());
        System.out.println(car2.getPrice());
        System.out.println();
        // car3
        System.out.println(car3.getBrand());
        System.out.println(car3.getMake());
        System.out.println(car3.getPrice());
        System.out.println();

        Driver driver1 = new Driver("Alejandro",25);

        car1.start();
        car2.start();
        car3.start();
        System.out.println();

        Driver newDriver = new Driver("Antonio",21);
        car1.setCarDriver(newDriver);
        System.out.println(car1.getCarDriver().getName());
        System.out.println(car1.getCarDriver().getAge());
        System.out.println(car1.getCarDriverName());
        System.out.println();

        Animal cat = new Animal("Cat","White");
        System.out.println(cat.getName());
        System.out.println(cat.getColor());
        cat.call();
        System.out.println();

        Dog dog1 =  new Dog();
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setName("Hachiko");
        System.out.println(dog1.getName());
        dog1.call();
        dog1.setColor("Brown");
        System.out.println(dog1.getColor());
        System.out.println();

        Dog dog2 =  new Dog("Mike", "Dark Brown", "Corgi");
        dog2.call();
        System.out.println(dog2.getName());

        System.out.println(dog2.getDogBreed());
        dog2.greet();
    }
}
