package com.zuitt;

import java.util.Date;
import java.util.GregorianCalendar;

public class Main {
    public static void main(String[] args) {
        User user1 = new User("John","john@mail.com","Quezon City",25);

        Course course1 = new Course();
        course1.setName("MACQ004");
        course1.setDescription("An Introduction to Java for career-shifters.");
        course1.setFee(3000.00);
        course1.setSeats(30);
        course1.setStartDate(new Date());
        course1.setEndDate(new GregorianCalendar(2023,7,30).getTime());
        course1.setInstructor(user1);

        // Output 1:
        System.out.println("Hi! I'm " + user1.getName() + ". I'm " + user1.getAge() + " years old. " +
                "You can reach me via my email: " + user1.getEmail() + ". When I'm off work, " +
                "I can be found at my house in " + user1.getAddress() + ".");
        System.out.println();

        // Output 2:
        System.out.println("Welcome to the course " + course1.getName() + ". This course can be described as " +
                course1.getDescription() + " Your instructor for this course is Sir " + course1.getInstructor().getName() +
                ". Enjoy!");
        System.out.println();

        System.out.println("Course " + course1.getName() + " will end on " + course1.getEndDate() + ".");
    }
}
