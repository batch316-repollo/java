package com.zuitt.example;

public interface Actions {
    // Interfaces are used to achieve abstraction. It hides away the actual implementation of the methods
    // and simply shows a Lists / a menu of methods that can be done.
    // Interfaces are like blueprints for your classes because any class that implements our interface
    // MUST have the methods in the interface.
    public void sleep();
    public void run();
    public void eat();
    public void code();
}
