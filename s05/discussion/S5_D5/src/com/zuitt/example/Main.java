package com.zuitt.example;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person();
        person1.eat();
        person1.run();
        person1.code();
        person1.sleep();
        System.out.println();

        StaticPoly staticPoly = new StaticPoly();
        System.out.println(staticPoly.addition(1,5));
        System.out.println(staticPoly.addition(1,5,10));
        System.out.println(staticPoly.addition(15.5,15.6));
        System.out.println();

        Parent parent = new Parent();
        parent.speak();
        Parent childOfParent = new Child();
        childOfParent.speak();
        System.out.println();

        parent.greet();
        parent.greet("Arvin","Morning");
    }
}
