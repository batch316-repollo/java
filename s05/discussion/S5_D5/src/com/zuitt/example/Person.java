package com.zuitt.example;

public class Person implements Actions {
    public void sleep() {
        System.out.println("Zzzzzz...");
    }
    public void run() {
        System.out.println("Running on the road.");
    }
    public void eat() {
        System.out.println("Now eating meal.");
    }
    public void code() {
        System.out.println("Coding Java and JavaScript.");
    }
}
