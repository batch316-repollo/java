package com.zuitt;

public class Contact {
    String name;
    String[] contactNumbers;
    String[] addresses;

    public Contact() {}
    public Contact(String name, String[] contactNumber, String[] addresses) {
        this.name = name;
        this.contactNumbers = contactNumber;
        this.addresses = addresses;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String[] getContactNumbers() {
        return contactNumbers;
    }

    public void setContactNumbers(String[] contactNumbers) {
        this.contactNumbers = contactNumbers;
    }

    public String[] getAddresses() {
        return addresses;
    }

    public void setAddresses(String[] addresses) {
        this.addresses = addresses;
    }
}
