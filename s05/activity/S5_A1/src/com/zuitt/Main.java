package com.zuitt;

public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("John Doe", new String[] {"+639152468596", "+639228547963"},
                new String[] {"My home in Quezon City.", "My office in Makati City."});
        Contact contact2 = new Contact("Jane Doe", new String[] {"+639162148573", "+639173698541"},
                new String[] {"My home in Caloocan City.", "My office in Pasay City."});

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        phonebook.displayAllContacts();
    }
}
