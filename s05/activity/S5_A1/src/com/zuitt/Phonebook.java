package com.zuitt;

import java.util.ArrayList;

public class Phonebook {
    ArrayList<Contact> contacts = new ArrayList<>();

    public Phonebook() {}
    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        this.contacts.add(contact);
    }

    public void displayAllContacts() {
        if (contacts.isEmpty()) {
            System.out.println("You have no contacts inside your Phonebook!");
        }
        else {
            contacts.forEach(contact -> {
                System.out.println("==================================================");
                System.out.println(contact.getName());
                System.out.println("--------------------------");
                System.out.println(contact.getName() + " has the following registered numbers:");
                for (String number : contact.getContactNumbers()) {
                    System.out.println(number);
                }
                System.out.println("--------------------------");
                System.out.println(contact.getName() + " has the following registered addresses:");
                for (String address : contact.getAddresses()) {
                    System.out.println(address);
                }
            });
            System.out.println("==================================================");
        }
    }
}
