package com.zuitt.example;

public class Loops {

    public static void main(String[] args) {
        // For loops
        for (int i = 0; i < 10; i++) {
            System.out.println("Current count: " + i);
        }

        System.out.println();

        int[] intArray = {100,200,300,400,500};
        for (int j = 0; j < intArray.length; j++) {
            System.out.println(intArray[j]);
        }

        System.out.println();

        // Another way to loop through arrays is called the forEach or the enhanced for loop.
        String[] nameArray = {"John", "Paul", "George", "Ringo"};
        for (String name : nameArray) {
            System.out.println(name);
        }

        System.out.println();

        String[][] classroom = new String[3][3];
        //First row
        classroom[0][0] = "Athos";
        classroom[0][1] = "Porthos";
        classroom[0][2] = "Aramis";
        //Second row
        classroom[1][0] = "Brandon";
        classroom[1][1] = "JunJun";
        classroom[1][2] = "Jobert";
        //Third row
        classroom[2][0] = "Mickey";
        classroom[2][1] = "Donald";
        classroom[2][2] = "Goofy";

        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                System.out.println(classroom[row][col]);
            }
        }

        System.out.println();

        int x = 0, y = 10;
        while (x < 10) {
            System.out.println("While Loop number: " + x);
            x++;
        }

        System.out.println();

        do {
            System.out.println("Countdown: " + y);
        }
        while (y > 10);
    }
}
