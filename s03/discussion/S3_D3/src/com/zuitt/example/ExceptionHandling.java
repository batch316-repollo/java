package com.zuitt.example;

import java.util.*;

public class ExceptionHandling {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Input a number: ");
        int num = 0;

        try {
            /*
             * nextInt() - expects an Integer.
             * nextDouble() - expects a double.
             * nextLine() - gets the entire line as String.
             * */
            num = sc.nextInt(); // The nextInt() method tells Java that it is expecting an integer value as input.
        }
        catch (Exception e) {
            System.out.println("Invalid input!");
            e.printStackTrace();
        }

        System.out.println("You have entered: " + num);
    }
}
