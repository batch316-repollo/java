package com.zuitt;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        int answer = 1;
        int counter = 1;
        int num = 0;

        System.out.println("Input an integer whose factorial will be computed.");

        try {
            num = in.nextInt();

            if (num < 0) {
                System.out.println("Factorial of negative numbers do not exist.\n" +
                        "Must enter an integer higher or equal than 0.");
            }
            else {
                answer = num == 0 ? 1 : ansInWhileLoop(num, counter);
                int answerInForLoop = ansInForLoop(num, counter);

                // To check if while loop and for loop method has same answer.
                //System.out.println(answer);
                //System.out.println(answerInForLoop);

                answer = answer == answerInForLoop ? answerInForLoop : answer;
                System.out.println("The factorial of " + num + " is " + answer);
            }
        }
        catch (Exception e) {
            System.out.println("Invalid input! Please enter an integer.");
            e.printStackTrace();
        }
        finally {
            // Print Stretch Goal.
            stretchGoal();
        }
    }

    private static int ansInForLoop(int num, int counter) {
        int answer = 1;

        for (int c = counter; c <= num; c++) {
            answer = answer * c;
        }

        return answer;
    }

    private static int ansInWhileLoop(int num, int counter) {
        int answer = 1;

        while (num > counter) {
            answer = answer * num;
            num--;
        }

        return answer;
    }

    private static void stretchGoal() {
        System.out.println("\nStretch Goal Display:");
        int numberOfAsterisk = 5;
        for (int a = 0; a < numberOfAsterisk; a++) {
            for (int b = 0; b <= a; b++) {
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println();
    }
}
